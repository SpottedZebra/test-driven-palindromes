module.exports = (str) => {
    const cleanedInput = str.replace(/\W/gi, "").toLowerCase();
    if (cleanedInput === cleanedInput.split('').reverse().join('')) return [cleanedInput]
    return [];
};
